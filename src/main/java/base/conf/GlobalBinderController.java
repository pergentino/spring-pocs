package base.conf;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import base.web.controller.rest.APIController;
import base.web.dto.RequestDTO;

@ControllerAdvice
public class GlobalBinderController {

	private Logger logger = LoggerFactory.getLogger(APIController.class);

	@InitBinder
	public void initBind(WebDataBinder binder) {

		logger.info("Binding on Configuration: {}", binder.getTarget());
		binder.registerCustomEditor(String.class, new PropertyEditorSupport() {

			@Override
			public Object getValue() {
				logger.info("getValue(): {}", getValue());
				return super.getValue();
			}

			@Override
			public String getAsText() {
				logger.info("getAsText(): {}", getAsText());
				return super.getAsText();
			}

			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				super.setAsText(text);
				logger.info("setAsText(): {}", text);
			}

		});
		
		binder.registerCustomEditor(RequestDTO.class, new PropertyEditorSupport() {

			@Override
			public Object getValue() {
				logger.info("RequestDTO getValue(): {}", getValue());
				return super.getValue();
			}

			@Override
			public String getAsText() {
				logger.info("RequestDTO getAsText(): {}", getAsText());
				return super.getAsText();
			}

			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				super.setAsText(text);
				logger.info("RequestDTO setAsText(): {}", text);
			}

		});
		
	}

}
