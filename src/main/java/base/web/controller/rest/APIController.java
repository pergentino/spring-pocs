package base.web.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import base.web.dto.RequestDTO;
import base.web.service.DelayService;

@RestController
@RequestMapping("/api")
public class APIController {
	
	private Logger logger = LoggerFactory.getLogger(APIController.class);
	
	@Autowired
	private DelayService delayService;
	
    @GetMapping
	@RequestMapping("/delay/{time}")
    @ResponseBody
	public String processDelay(@PathVariable Long time) {
    	
    	logger.info("Request received {}.", time);
		String delayProcessed = delayService.processDelay(time);
		logger.info("Processed delay {}: {}.", time, delayProcessed);
    	
		return "Done";
	}

    @GetMapping("/binding")
    @ResponseBody
	public ResponseEntity<?> processBinder(@ModelAttribute RequestDTO dto) {
    	
    	Assert.notNull(dto, "DTO object is required.");
    	Assert.hasText(dto.getText(), "Text is required.");
    	
    	logger.info("Request received for {}.", dto);
    	
		return ResponseEntity.ok("Text: "+ dto.getText());
	}
    
}