# Base project containing the following technologies:

## Spring Boot 2.7.18 (spring-boot-starter-parent)

Created an `IndexController` as an example.

Created an `APIController` to test the InitBinder in a Controller level.

Configured with spring profiles (VM arg `-Dspring.profiles.active=dev`).

Properties files following the pattern `application-{ENV}.properties`

Logging with `logback-spring.xml` using spring profiles

## To test the InitBinder, run the application and configure an equivalent request on Postman to reflect the following cURL call:

```
curl --location --request GET 'http://localhost:8080/api/binding' \
--header 'Content-Type: application/json' \
--data '{
    "text": "Teste"
}'
```

Request Method: GET

Request URL: http://localhost:8080/api/binding

Body Payload JSON:

```
{
    "text": "Teste"
}
```